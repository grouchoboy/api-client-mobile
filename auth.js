
class Auth {
  static login(user, pass, cb) {
    const credentials = window.btoa(`${user}:${pass}`);
    const req = new XMLHttpRequest();
    req.open('GET', 'http://localhost:3000/users/login', true);
    req.setRequestHeader('Authorization', `Basic ${credentials}`);
    req.send();

    req.onload = () => {
      if (req.status === 200) {
        localStorage.username = user;
        localStorage.password = pass;
        localStorage.loggedIn = true;
        if (cb) cb(true);
        this.onChange(true);
      }
    }
  }

  static logout() {
    delete localStorage.user;
    delete localStorage.pass;
    delete localStorage.loggedIn;
    this.onChange(false);
  }

  static getCredentials() {
    return {
      username: localStorage.user,
      password: localStorage.pass,
    }
  }

  static loggedIn() {
    return !!localStorage.loggedIn;
  }

  static onChange() {}
}

export default Auth;