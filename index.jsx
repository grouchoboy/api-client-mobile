import React from 'react';
import { render } from 'react-dom';
import { Router, Route, hashHistory } from 'react-router';
import App from './components/app';
import Users from './components/users';
import Films from './components/films';
import Film from './components/film';
import Reviews from './components/reviews';
import User from './components/user';
import Login from './components/login';
import Logout from './components/logout';

render((
  <Router history={hashHistory}>
    <Route path="/" component={App}>
      <Route path="/login" component={Login} />
      <Route path="/logout" component={Logout} />
      <Route path="/films" component={Films} />
      <Route path="films/:id" component={Film} />
      <Route path="/users" component={Users} />
      <Route path="/users/:username" component={User} />
      <Route path="/reviews" component={Reviews} />
    </Route>
  </Router>
),document.getElementById('app'));

