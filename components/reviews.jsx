import React from 'react';

class Reviews extends React.Component {
  state = {
    data: []
  };



  render() {
    const reviewNodes = this.state.data.map(review => {
      return (
        <li key={review.title}>
          {review.title}
        </li>
      )
    });

    return(
      <ul>
        {reviewNodes}
      </ul>
    )
  }
}

export default Reviews;
