import React from 'react';
import UserNode from './userNode';

const Users = React.createClass({
  getInitialState() {
    return { data: [] };
  },

  loadUsersFromServer() {
    const endpoint = 'http://localhost:3000/users';

    const p = fetch(endpoint);
    p.then(function(response) {
      const jsonPromise = response.json();
      jsonPromise.then(function(json) {
        this.setState({ data: json.data });
      }.bind(this));
    }.bind(this));
    p.catch(function(error) {
      console.log(error.message);
    });
  },

  componentDidMount() {
    this.loadUsersFromServer();
  },

  render() {
    const userNodes = this.state.data.map(user => {
      return(
        <li key={user.username}>
          <UserNode username={user.username} description={user.description} />
        </li>
      );
    });

    return(
      <ul className="list">
        {userNodes}
      </ul>
    );
  }
});

export default Users;