import React from 'react';
import FilmModel from '../models/film-model';
import Auth from '../auth';

class Film extends React.Component {

  state = {
    id: '',
    title: '',
    description: '',
    titleForm: '',
    bodyForm: '',
  }

  componentDidMount() {
    const p = FilmModel.getByTitle(this.props.params.id);
    p.then(function(response) {
      this.setState({
        id: response.id,
        title: response.title,
        description: response.description,
      });
    }.bind(this));
  }

  handleSubmit = (event) => {
    event.preventDefault();
    if (Auth.loggedIn()) {
      const user = Auth.getCredentials();
      const req = new XMLHttpRequest();
      req.open('POST', "http://localhost:3000/reviews", true);
      const credentials = window.btoa(`${user.username}:${user.password}`);
      req.setRequestHeader('Authorization', `Basic ${credentials}`);
      req.setRequestHeader('Content-Type', 'application/json');
      const params = {
        filmId: this.state.id,
        title: this.state.titleForm,
        body: this.state.bodyForm,
      }
      req.send(JSON.stringify(params));
      req.onload = () => {
        if (req.status == 200) {
          console.log(req);
        } else {
          console.log(req);
        }
      }
    }

  }

  handleTitleChange = (event) => {
    event.preventDefault();
    this.setState({
      titleForm: event.target.value,
    });
  }

  handleBodyChange = (event) => {
    event.preventDefault();
    this.setState({
      bodyForm: event.target.value,
    });
  }

  render() {
    return(
      <div className="element">
        <h2>{this.state.title}</h2>
        <span>{this.state.description}</span>

        <form>
          <div>
            <label htmlFor="title">Tu Opinión en una frase</label>
            <input type="text" name="title"
              onChange={this.handleTitleChange}
              placeholder="estupenda"/>
          </div>

          <div>
            <label htmlFor="body">Explica la frase anterior</label>
            <textarea name="title" cols="30"
              onChange={this.handleBodyChange}
              rows="10"></textarea>
          </div>

          <div>
            <button type="submit" onClick={this.handleSubmit}>Enviar</button>
          </div>
        </form>
      </div>
    )
  }
}

export default Film;