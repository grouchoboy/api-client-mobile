import React from 'react';
import FilmNode from './filmNode';
import FilmModel from '../models/film-model';

class Films extends React.Component {
  state = {
    data: []
  };

  loadFilmsFromServer() {
    const p = FilmModel.getAll();
    p.then(function(response) {
      this.setState({
        data: response.data,
      });
    }.bind(this));
    };

    componentDidMount() {
      this.loadFilmsFromServer();
    };

    render() {
      const filmNodes = this.state.data.map(function(film) {
        return(
          <li key={film.title}>
            <FilmNode title={film.title} description={film.description} id={film.id} />
          </li>
        )
      });

      return(
        <ul className="list">
          {filmNodes}
        </ul>
      )
    };
}

export default Films;