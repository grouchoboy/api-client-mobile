import React from 'react';
import { Link } from 'react-router';
import Auth from '../auth';

const App = React.createClass({
  getInitialState() {
    return {
      loggedIn: Auth.loggedIn(),
    }
  },

  updateAuth(loggedIn) {
    this.setState({
      loggedIn: loggedIn,
    });
  },

  componentWillMount() {
    Auth.onChange = this.updateAuth;
  },


  render() {
    return (
      <div>
        <div id="title-container"><h1>MovieApp</h1></div>
        <nav id="navigation">
          <ul role="nav">
            <li><Link className="nav-item" to="/films">Peliculas</Link></li>
            <li><Link className="nav-item" to="/users">Usuarios</Link></li>
            {this.state.loggedIn ? (
              <li><Link className="nav-item" to="/logout">Logout</Link></li>
            ) : (
              <li><Link className="nav-item" to="/login">Entra</Link></li>
            )}
          </ul>
        </nav>

        {this.props.children}
      </div>
    )
  }
});

export default App;
