import React from 'react';
import UserModel from '../models/user-model';

class User extends React.Component {

  state = {
    id: '',
    username: '',
    description: '',
    reviews: [],
  }

  loadUserFromServer() {
    const p = UserModel.getByName(this.props.params.username);

    p.then(function (user) {
      this.setState({
        id: user.id,
        username: user.username,
        description: user.description,
      });
    }.bind(this));
  }

  loadReviewsFromServer() {
    const user = new UserModel();
    user.username = this.props.params.username;
    const p = user.getReviews();
    p.then(function(reviews) {
      console.log(reviews);
      this.setState({
        reviews: reviews,
      });
    }.bind(this));
  };

  componentDidMount() {
    this.loadUserFromServer();
    this.loadReviewsFromServer();
  }

  render() {
    const reviewNodes = this.state.reviews.map(function (review) {
      return(
        <li key={review.id}>
          <h4>{review.film}</h4>
          <span className="review-title">{review.title}</span>
          <p>{review.body}</p>
        </li>
      )
    });

    return(
      <div className="element">
        <h2>{this.state.username}</h2>
        <span>{this.state.description}</span>
        <h3>Reviews</h3>
        <ul className="reviews">{reviewNodes}</ul>
      </div>
    )
  }
}

export default User;