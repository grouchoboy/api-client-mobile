import React from 'react';
import Auth from '../auth';

class Login extends React.Component {
  static contextTypes = {
    router: React.PropTypes.object,
  }

  state = {
    username: '',
    password: '',
  }

  handleSubmit = (event) => {
    event.preventDefault();
    Auth.login(this.state.username, this.state.password, (loggedIn) => {
      if (loggedIn) {
        this.context.router.push(`/users/${this.state.username}`);

      }
    });
  }

  handleUsernameChange = (e) => {
    this.setState({
      username: e.target.value,
    });
  }

  handlePasswordChange = (e) => {
    this.setState({
      password: e.target.value,
    });
  }

  render() {
    return(
      <div>
        <from>
          <h1>Entra</h1>
          <div>
            <label htmlFor="username">Nombre de usuario</label>
            <input type="text" name="username"
            onChange={this.handleUsernameChange}
            placeholder="tu nombre de usuario" />
          </div>

          <div>
            <label htmlFor="password">Contraseña</label>
            <input type="password" name="password"
            onChange={this.handlePasswordChange}
             placeholder="tu contraseña" />
            }
          </div>

          <div>
            <button id="login" type="submit" onClick={this.handleSubmit}>
              Entra
            </button>
          </div>
        </from>
      </div>
    )
  }
}

export default Login;