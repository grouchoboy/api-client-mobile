import React from 'react';
import { Link } from 'react-router';

const UserNode = React.createClass({
  render() {
    return(
      <article>
        <h3><Link to={`/users/${this.props.username}`}>{this.props.username}</Link></h3>
        <p>{this.props.description.substring(0, 255)}...</p>
      </article>
    )
  }
});

export default UserNode;