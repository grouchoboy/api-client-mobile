import React from 'react';
import { Link } from 'react-router';

const FilmNode = React.createClass({

  render() {
    return(
      <article>
        <h3><Link to={`/films/${this.props.id}`}>{this.props.title}</Link></h3>
        <p>{this.props.description}</p>
      </article>
    )
  }
});

export default FilmNode;