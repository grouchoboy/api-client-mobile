import React from 'react';
import Auth from '../auth';

class Logout extends React.Component {
  handleSubmit(event) {
    event.preventDefault();
    Auth.logout();
  }

  render() {
    return <button onClick={this.handleSubmit}>Salir</button>
  }
}

export default Logout;