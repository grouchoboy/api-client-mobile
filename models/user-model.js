
class UserModel {
  get id() {
    return this._id;
  }

  set id(id) {
    this._id = id;
  }

  get username() {
    return this._username;
  }

  set username(name) {
    this._username = name;
  }

  get description() {
    return this._description;
  }

  set description(description) {
    this._description = description;
  }

  toString() {
    return `Hello my name is ${this._username}`;
  }

  static getByName(username) {
    const promise = new Promise(function(resolve, reject) {
      const req = new XMLHttpRequest();
      req.open('GET', `http://localhost:3000/users/${username}`);
      req.send();

      req.onload = function() {
        if (this.status === 200) {
          const data = JSON.parse(this.response);
          const user = new UserModel();
          user.id = data.id;
          user.username = data.username;
          user.description = data.description;
          resolve(user);
        } else {
          reject(this.statusText);
        }
      }
    });

    return promise;
  }

  getReviews() {
    const promise = new Promise(function(resolve, reject) {
      const req = new XMLHttpRequest();
      req.open('GET', `http://localhost:3000/users/${this.username}/reviews`);
      req.send();
      req.onload = function() {
        if (this.status === 200) {
          const reviews = JSON.parse(this.response);
          resolve(reviews);
        } else {
          reject(this.status);
        }
      }
    }.bind(this));

    return promise;
  }
}

export default UserModel;