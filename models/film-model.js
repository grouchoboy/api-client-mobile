class FilmModel {

  static getByTitle(id) {
    const promise = new Promise(function(resolve, reject) {
      const req = new XMLHttpRequest();
      req.open('GET', `http://localhost:3000/films/${id}`);
      req.send();

      req.onload = function() {
        if (this.status === 200)  {
          resolve(JSON.parse(this.response));
        } else {
          reject(this.statusText);
        }
      }
    });

    return promise;
  }

  static getAll() {
    const promise = new Promise(function(resolve, reject) {
      const req = new XMLHttpRequest();
      req.open('GET', 'http://localhost:3000/films');
      req.send();

      req.onload = function() {
        if (this.status == 200) {
          const data = JSON.parse(this.response);
          resolve(data);
        }
        resolve(this);
      }
    });

    return promise;
  }
}

export default FilmModel;